# PAGE D'ERREUR CUSTOMISÉE

## Description

Cette page va permettre de customiser la page d'erreur de Synology utilisée par défaut.

![Page d'erreur par défaut](./doc/default_error_page.jpg)

## Versions

Il y a deux versions différentes de la page d'erreur customisée.

### Version originale

La version originale est juste un aperçu du la page final mais avec un développement classique.
Elle est composée d'une [page HTML](./src/error.html) qui a des dépendances vers une [page de style](./src/style.css) et vers des [images](./src/images).
[Voir la version originale](./src)

### Version condensée

La version condensée reprend le code de la version originale mais le condense dans un seul fichier _HTML_.
C'est ce fichier qui remplacera la page d'erreur par défaut.
[Voir la version originale](./synosrc)

Dans la version condensé, les images doivent être encodée en base 64.
J'ai utilisé ce [site](https://www.base64-image.de) pour la conversion.

## Déploiement

1. Déposer la version condensée `error.html`

    Pour ma part, je me suis connecté sur le _DSM_ puis j'ai déposé mon fichier dans `/volume1/web`.

2. Mettre à jour le fichier `error.html`

    Il faut se connecter en _SSH_ sur le NAS.

    ```
    ssh administrateur@adresse_ip_nas
    ```

    Une fois connecté il faut remplacer le fichier `error.html` de base avec la version condensée.

    ```
    sudo mv /volume1/web/error.html /usr/syno/share/nginx/error.html -f
    ```

Si tous c'est bien passé, la page a été modifiée!

![Page d'erreur customisée](./doc/custom_error_page.JPG)